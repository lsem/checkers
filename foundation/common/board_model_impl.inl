

namespace checkers
{
    // Holds all implementation specific data and associated operations of class BoardModel.
    // This technique is used for hiding implementation details of class so that class header
    // does not have undesirable dependencies on other libraries, headers, etc..
    // In typical scenario class creates implementation in its constructor and removes it on destructor.
    class BoardModel::BoardModel_Implementation
    {
    public:
        struct Direction 
        {
            Direction(int row, int col, bool attacks_only=false) 
                : row(row), col(col), attacks_only(attacks_only) 
            {}
            int row;
            int col;
            bool attacks_only; // indicates only attacks allowed in this direction
        };        

        typedef std::set<BoardPos> BoardPositionsContainer;
        typedef std::vector<Cell> CellsContainer;        
        typedef std::list<Direction> DirectionsList;


        BoardModel_Implementation(unsigned rows, unsigned columns);

        void do_make_move(const BoardPos& from, const Move& move, bool attack_mode);

        void do_make_attack(const BoardPos& from, const Move& move, bool attack_mode);        

        // Finds first move in specified collection which meets specified requirements.
        // Returns true if found 
        static bool find_move_in_collection(const MovesCollection& coollection, const BoardPos& from, const BoardPos& to, 
            const MoveType& kind, MovesCollection::const_iterator &out_iterator);

        static bool is_black_pos(unsigned row, unsigned column) { return (row + column) % 2 == 0; }

        static bool is_white_pos(unsigned row, unsigned column) { return !is_white_pos(row, column); }

        Cell& cell_at(size_t offset);

        const Cell& cell_at(size_t offset) const;

        const Cell& cell_at(const BoardPos& position) const;
        Cell& cell_at(const BoardPos& position);       

        // Reset board: remove all figures and init cells.
        void reset_board_layout();

        // Set figures for new game.
        void reset_figures();

        bool in_board(const BoardPos& ) const;

        DirectionsList get_possible_directions(const BoardPos& from) const;              

        // Returns pair of (white, black) information about diagonal (how many white and how many black figures)
        std::pair<int, int> get_diagonal_traits(const BoardPos& from, const BoardPos& to) const;

        static bool on_same_diagonal(const BoardPos& a, const BoardPos& b);

        void find_moves_for_direction(const BoardPos& from, const Direction&, unsigned max_distance, MovesCollection& accumulator) const;

        // Removes Move moves if there are any Attack moves
        static void filter_out_incompatible_moves(MovesCollection& inout_collection, bool attacks_only);

        static const unsigned k_num_player_rows = 3;

        unsigned _rows;
        unsigned _columns;
        unsigned _black_figures_count; // holds a count of white figures on the board.
        unsigned _white_figures_count; // holds a count of black figures on the board.
        CellsContainer _board_data;
    };


    /////////////////// class  BoardModel::BoardModel_Implementation ///////////////////////////

    BoardModel::BoardModel_Implementation::BoardModel_Implementation(unsigned rows, unsigned columns)
        : _rows(rows)
        , _columns(columns)
        , _black_figures_count(0)
        , _white_figures_count(0)
    {
        _board_data.resize(_rows * _columns);            
    }

    void BoardModel::BoardModel_Implementation::reset_board_layout()
    {
        for (unsigned i = 0; i < _rows; ++i)
        {
            for (unsigned j = 0; j < _columns; ++j)
            {
                Cell cell = ((i + j)% 2 == 0)? 
                    Cell::get_empty_black() 
                    : Cell::get_empty_white();
                cell_at(BoardPos(i, j)) = cell;
            }
        }
    }

    void BoardModel::BoardModel_Implementation::reset_figures()
    {        
        _white_figures_count = 0;
        _black_figures_count = 0;
        // set up figures for player 1 (white checkers on black cells)
        for (unsigned i = 0; i < k_num_player_rows; ++i)
        {
            for (unsigned j = 0; j < _columns; ++j)
            {
                if ( (i + j) % 2 == 0)
                { // on black cell                    
                    cell_at(BoardPos(i, j)).put_white_figure();
                    _white_figures_count++;
                    // symmetrically put black figure on the opposite side
                    cell_at(BoardPos(_rows - 1 - i, _columns - 1 - j)).put_black_figure();
                    _black_figures_count++;
                }
            }            
        }
    }


    const Cell& BoardModel::BoardModel_Implementation::cell_at(const BoardPos& position) const
    {
        return cell_at(position.row() * _columns + position.column());    
    }

    const Cell& BoardModel::BoardModel_Implementation::cell_at(size_t offset) const
    {
        return _board_data.at(offset);
    }

    Cell& BoardModel::BoardModel_Implementation::cell_at(const BoardPos& position) 
    {
        return cell_at(position.row() * _columns + position.column());
    }

    Cell& BoardModel::BoardModel_Implementation::cell_at(size_t offset)
    {
        return _board_data.at(offset);
    }

    bool BoardModel::BoardModel_Implementation::in_board(const BoardPos& pos) const
    {
        return pos.row() >= 0 && pos.column() >= 0 &&
            pos.row() < _rows && pos.column() < _columns;
    }

    BoardModel::BoardModel_Implementation::DirectionsList 
        BoardModel::BoardModel_Implementation::get_possible_directions(const BoardPos& from) const
    {        
        BoardModel::BoardModel_Implementation::DirectionsList result;
        int vdirection = cell_at(from).is_white_figure()? 1 : -1;
        result.push_back(Direction(vdirection, +1));
        result.push_back(Direction(vdirection, -1));        
        // only attacks allowed backward, queens can move backward too
        bool attacks_only = !cell_at(from).is_queen();
        result.push_back(Direction(-vdirection, +1, attacks_only));
        result.push_back(Direction(-vdirection, -1, attacks_only));
        return result;
    }

    bool BoardModel::BoardModel_Implementation::on_same_diagonal(const BoardPos& a, const BoardPos& b)
    {
        int delta_rows = a.row() - b.row();
        int delta_cols = a.column() - b.column();
        return delta_rows == delta_cols;
    }

    // Finds all moves in specified direction from specified position up to max_distance and taking into consideration if it is attack mode.    
    //   Argument max_distance is a maximum distance for this figure (not taking into consideration figure position)
    void BoardModel::BoardModel_Implementation::find_moves_for_direction(const BoardPos& from, const Direction& direction, 
        unsigned max_distance, MovesCollection& accumulator) const
    {
        bool attack_mode = false; 
        bool stop_search = false;

        BoardPos enemy_pos;
        for (unsigned distance = 1; !stop_search && distance <= max_distance; ++distance)
        {
            BoardPos target_pos(from.row() + distance * direction.row, 
                from.column() + distance * direction.col);                            
            if (!(stop_search = !in_board(target_pos)))
            {
                if (cell_at(target_pos).is_empty())
                { // target position is empty
                    if (!is_valid(enemy_pos))
                    { // not jumped over enemy before and not attack only mode for this direction
                        if (!direction.attacks_only)
                        {
                            accumulator.add(Move::construct_move(target_pos));
                        }
                    }
                    else
                    { // found empty and previously jumped over some enemy ~ attack move found 
                        accumulator.add(Move::construct_attack(target_pos, enemy_pos)); // there is also information WHICH figure is attacked
                        // need to stop after first attack
                        stop_search = true;
                    }
                }
                else if(cell_at(target_pos).is_enemy_to(cell_at(from)))
                { // encountered enemy figure 
                    // just save the position where we found it
                    enemy_pos = target_pos;
                    max_distance += 1; // allow one more move
                }
                else if (cell_at(target_pos).is_ally_to(cell_at(from)))
                { // encounter ally figure
                    // cannot jump over ally figure
                    stop_search = true;
                }            
            }
        }
    }

    void BoardModel::BoardModel_Implementation::filter_out_incompatible_moves(MovesCollection& inout_collection, bool attacks_only)
    {
        MovesCollection temp_collection;
        MovesCollection::const_iterator mit, rcend = inout_collection.end();
        bool attacks_present = false;
        for (mit = inout_collection.begin(); !attacks_present && mit != rcend; ++mit)
        {
            attacks_present  = mit->type() == MoveType_Attack;
        }        
        for (mit = inout_collection.begin(); mit != rcend; ++mit)
        {
            bool filtered = mit->type() == MoveType_Move && (attacks_only || attacks_present);
            if (!filtered)
            {
                temp_collection.add(*mit);
            }
        }
        inout_collection.swap(temp_collection);
    }

    bool BoardModel::BoardModel_Implementation::find_move_in_collection(const MovesCollection& coollection, 
        const BoardPos& from, const BoardPos& to, const MoveType& kind, MovesCollection::const_iterator &out_iterator)
    {
        bool result = false;
        MovesCollection::const_iterator mit, end = coollection.end();
        bool found_allowed = false;
        for (mit = coollection.begin(); !found_allowed && mit != end; ++mit)
        {
            found_allowed = mit->position_to() == to && mit->type() == kind;
        }
        out_iterator = mit;
        result = found_allowed;
        return result;
    }

    void BoardModel::BoardModel_Implementation::do_make_move(const BoardPos& from, const Move& move, bool attack_mode)
    {
        assert(move.type() == MoveType_Move || move.type() == MoveType_Attack); // can be used by attack function
        cell_at(from).is_black_cell()?
            cell_at(move.position_to()).put_black_figure() :
            cell_at(move.position_to()).put_white_figure();
        cell_at(from).remove_figure();
    }

    void BoardModel::BoardModel_Implementation::do_make_attack(const BoardPos& from, const Move& move, bool attack_mode)
    {
        assert(move.type() == MoveType_Attack);
        do_make_move(from, move, attack_mode);
        assert(cell_at(move.attacked_figure()).is_figure());
        cell_at(move.attacked_figure()).is_black_figure()?
            --_black_figures_count :
        --_white_figures_count;
        cell_at(move.attacked_figure()).remove_figure();
    }


}