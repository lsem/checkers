#ifndef CH_BOARD_POSITION_H
#define CH_BOARD_POSITION_H
#include <climits>

namespace checkers
{
    class BoardPos
    {
    public:
        static const unsigned k_invalid_position = UINT_MAX;

        BoardPos();
        BoardPos(unsigned row, unsigned column)
            : _row(row)
            , _col(column)
        {}

        ~BoardPos() {}

        bool operator<(const BoardPos& other) const;
        bool operator==(const BoardPos& other) const;
        bool operator!=(const BoardPos& other) const;

        void set_row(unsigned number);
        void set_col(unsigned number);

        unsigned row() const { return _row; }
        unsigned column() const {return _col; }
        
    private:        
        unsigned _row;
        unsigned _col;
    };

    inline BoardPos::BoardPos() 
        : _row(k_invalid_position)
        , _col(k_invalid_position)
    {
    }

    inline bool is_valid(const BoardPos& boardpos)
    {
        return boardpos.row() != BoardPos::k_invalid_position 
            && boardpos.column() != BoardPos::k_invalid_position;
    }

    inline bool BoardPos::operator<(const BoardPos& other) const
    {
        return _row < other.row() || (_row == other.row() && _col < other.column());
    }

    inline bool BoardPos::operator==(const BoardPos& other) const
    {
        return !(*this < other) && !(other < *this);
    }

    inline bool BoardPos::operator!=(const BoardPos& other) const
    {
        return ! operator==(other);
    }
}


#endif // CH_BOARD_POSITION_H
