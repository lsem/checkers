#include "events_collection.h"

namespace checkers
{
    EventsCollection::EventsCollection()
    {
    }

    EventsCollection::~EventsCollection()
    {
        free_events_objects();
    }

    void EventsCollection::free_events()
    {
        free_events_objects();
        _events.clear();
    }

    void EventsCollection::free_events_objects()
    {
        for (iterator epit = _events.begin(); epit != _events.end(); ++epit)
        {
            if (*epit)
            {
                delete *epit;
                *epit = NULL;
            }
        }
    }

    void EventsCollection::push_back(Event* event)
    {
        _events.push_back(event);
    }
}