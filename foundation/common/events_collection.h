#ifndef CH_EVENTS_COLLECTION_H
#define CH_EVENTS_COLLECTION_H
#include <vector>

#include "event.h"

namespace checkers
{
    // Class responsible for maintaining events object.     
	class EventsCollection
	{
	public:
		EventsCollection();
		~EventsCollection();

		typedef std::vector<Event*> collection_impl_t;
		typedef collection_impl_t::iterator iterator;
		typedef collection_impl_t::const_iterator const_iterator;

        // Returns iterator which points to first element in collection of element after the last if empty.
        iterator begin();
        
        // Returns non-const iterator which points to element after the last.
        iterator end();		
        
        // Returns const iterator which points to first element in collection of element after the last if empty.
        const_iterator begin() const;
		
        // Returns iterator which points to element after the last.
        const_iterator end() const;
                
        // Insert specified element at the and of the collection.
        void push_back(Event* event);		
        
        // Deleted owned event objects and clears collection so that after size becomes zero.        
        void free_events();		
        
        // Returns a count of owned event objects.
        size_t size() const;
	
    private:
		void free_events_objects();

		std::vector<Event*> _events;
	};

	inline EventsCollection::const_iterator EventsCollection::begin() const
	{
		return _events.begin();
	}

	inline EventsCollection::const_iterator EventsCollection::end() const
	{
		return _events.end();
	}

	inline size_t EventsCollection::size() const
	{
		return _events.size();
	}

    inline EventsCollection::iterator EventsCollection::begin() 
    {
        return _events.begin();
    }

    inline EventsCollection::iterator EventsCollection::end() 
    {
        return _events.end();
    }


}

#endif // CH_EVENTS_COLLECTION_H
