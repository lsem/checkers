#ifndef CH_BOARD_MODEL_H
#define CH_BOARD_MODEL_H
#include <cassert>
#include <vector>
#include <set>
#include <list>

#include "board_position.h"


namespace checkers
{    
    typedef int player_id_type;

    /*************************************************************************
      Class represents border cell.
    ***********************************************************************/
    class Cell
    {
    public:        
        // Constructs white cell with white figure put on.
        Cell()
            : _flags(0)
        {
        }

        // Family of functions which returns some traits of the cell
        bool is_empty() const { return (_flags & CellFlags_Empty)  != 0; }        
        bool is_figure() const { return !is_empty(); }
        bool is_queen() const { return is_figure() && (_flags & CellFlags_Queen)  != 0; }
        bool is_black_cell() const { return (_flags & CellFlags_Black) != 0; }
        bool is_white_cell() const { return !is_black_cell(); }
        bool is_white_figure() const { return is_figure() && !is_black_figure();  }
        bool is_black_figure() const { return is_figure() &&  (_flags & CellFlags_FigBlack)  != 0; }

        // Modifiers
        void make_queen() { assert(is_figure()); (_flags |= CellFlags_Queen);  }
        void remove_figure() { assert(is_figure()); (_flags |= CellFlags_Empty); }
        void make_empty() { remove_figure(); }
        void put_black_figure() { _flags &= ~CellFlags_Empty;  _flags |= CellFlags_FigBlack; }
        void put_white_figure() { _flags &= ~CellFlags_Empty;  _flags &= ~CellFlags_FigBlack; }

        // Family of function that create various valid cells
        static Cell get_empty_white() { return Cell(k_white_empty); }         
        static Cell get_empty_black()  { return Cell(k_black_empty); }
        
        // Returns whether specified figure is enemy to given figure.
        bool is_enemy_to(const Cell& other) const 
        {                         
            return is_figure() && ((_flags & CellFlags_FigBlack) ^ (other._flags & CellFlags_FigBlack)) != 0;
        }

        // Returns whether specified figure is ally for given figure.
        bool is_ally_to(const Cell& other) const { return is_figure() &&  !is_enemy_to(other); }

    private:
        Cell(int init_flags)
            : _flags(init_flags)
        {
        }
                        
        enum CellFlags
        {            
            CellFlags_Empty     = 0x01,     // whether is empty (1) of there is some figure (0)
            CellFlags_Black     = 0x04,     // Whether cell is black (1) of white (0)
            CellFlags_FigBlack  = 0x08,     // Whether figure is black(1) or white (0)            
            CellFlags_Queen     = 0x20,     // Indicates figure is queen (1) or regular figure (0)
        };
        
        static const int k_white_empty = CellFlags_Empty;
        static const int k_black_empty = CellFlags_Empty | CellFlags_Black;        
    
    private:
        int  _flags;
    };

    // Board knows rules and maintains game state.
    // Model accepts 
    class BoardModel
    {
    public:
        enum MoveType
        {
            MoveType_Invalid,
            MoveType_Move, // Regular (forward) move
            MoveType_Attack
        };

        // Represents move to some position.
        // Client code normally does not create objects of this class and should only use
        // moves returned by model. Means for construction may not be available in future releases.
        class Move
        {                        
            friend class BoardModel;
        
        public:            
            MoveType type() const;
            const BoardPos& position_to() const;
            const BoardPos& attacked_figure() const;
             
            // I would like disallow client code to create move objects itself. But
            // we need enable test models adapters to create moves and I didn't found any elegant solution
            // except of conditional compilation.
#ifndef CH_TESTS
        private:
#endif
            Move(MoveType type, const BoardPos& dest, const BoardPos& enemy=BoardPos()) 
                : _type(type), _destination(dest), _attacked_figure(enemy)  
            {}
        
        private: // BoardModel constructs moves with this methods
            static Move construct_attack(const BoardPos& dest, const BoardPos& enemy) ;
            static Move construct_move(const BoardPos& dest);

        private:
            MoveType _type;  
            BoardPos _destination;   // a destination position
            BoardPos _attacked_figure; // [optional] (makes sense if type is Attack)
        };
        

    public:
        class MovesCollection
        {
        private:
            typedef std::list<Move> Container;
        
        public:                        
            typedef Container::const_iterator const_iterator;

            void add(const Move& move);
            const_iterator begin() const ;
            const_iterator end() const ;
            void swap(MovesCollection& other);
            unsigned count() const ;
            unsigned motions_count() const;
            unsigned attacks_count() const;
        private:            
            Container _moves;
            unsigned _attacks;
        };
        

        // Constructs the board model. After construction board is ready for playing (figures are already set).
        // See also: reset() method.
        BoardModel(unsigned rows, unsigned columns);
        
        ~BoardModel();

        // Reset board so that board becomes prepared for new game.
        void reset(); 
        
        // Return const cell located at specified position. In case cell position is out of the board, std::out_of_range raised.
        const Cell& cell_at(const BoardPos& position) const;

        // Return cell located at specified position. In case cell position is out of the board, std::out_of_range raised.
        Cell& cell_at(const BoardPos& position);       

        // Returns a count of white figures currently located on the board.
        unsigned white_figures_count() const;
        
        // Returns a count of black figures currently located on the board.
        unsigned black_figures_count() const;        
                
        // Generates collection of moves available from specified position. Owner of figure is detected automatically.
                
        void get_possible_moves(const BoardPos& from, MovesCollection& out_moves, bool attacks_only) const;
        
        unsigned rows_count() const;
        
        unsigned columns_count() const;
        
        // Makes specified move. It is expected that specified move is valid as there should not be ability get move object
        // except from get_possible_moves() which have to do its job right.
        void make_move(const BoardPos& from, const Move& move, bool attack_mode);

        bool is_move_allowed(const BoardPos& from, const BoardPos& to, const MoveType& kind, bool attack_mode) const;
        
    private:
        class BoardModel_Implementation;
        BoardModel_Implementation *_impl;

    };

}

#endif // CH_BOARD_MODEL_H
