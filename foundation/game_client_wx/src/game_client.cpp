#include <iostream>
#include <Poco/Logger.h>
#include <QApplication>

#include "game.h"
#include "engine.h"
#include "qtengine.h"

int main(int argc, char* argv[])
{
    std::cout << "Creating engine.." << std::endl;
    checkers::Engine* engine = new checkers::QtEngine(argc, argv);
    std::cout << "Initializing window.." << std::endl;
    auto window = engine->init_window();
    std::cout << "Creating game.." << std::endl;
    checkers::Game* game = new checkers::Game(window->get_input());
    std::cout << "Starting game.." << std::endl;
    engine->run(*game);
    std::cout << "Finished" << std::endl;
}
