#include <iostream>
#include <cstdlib>
#include <iterator>

#include "logging_configuration.h"
#include <common/board_model.h>

using namespace playground;

    template <class OutputIterator>
    void make_collection(OutputIterator out_iterator)
    {
        *out_iterator++ = std::rand() % 1000;
    }


int main(int argc, char* argv[])
{
    configure_Logging();
    print_startup_banner();	

    std::vector<int> vi;
    
    make_collection(std::back_inserter(vi));
    make_collection(std::back_inserter(vi));
    make_collection(std::back_inserter(vi));
    make_collection(std::back_inserter(vi));
    make_collection(std::back_inserter(vi));

    poco_information(MODLOGGER, "Exiting playground ...");
}
