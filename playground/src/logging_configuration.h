#ifndef CH_LOGGING_CONFIGURATION_H
#define CH_LOGGING_CONFIGURATION_H

#include <Poco/Logger.h>
#include <Poco/SimpleFileChannel.h>
#include <Poco/PatternFormatter.h>
#include <Poco/Message.h>
#include <Poco/ConsoleChannel.h>
#include <Poco/FormattingChannel.h>
#include <Poco/AutoPtr.h>
#include <Poco/Format.h>

	
#define MODLOGGER (Logger::get("Playground"))


using Poco::AutoPtr;
using Poco::Logger;
using Poco::ConsoleChannel;
using Poco::PatternFormatter;
using Poco::FormattingChannel;

namespace playground 
{
	static const char* k_log_level = "debug";
	static const char* k_formatting_pattern = "%l %Y-%m-%d %H:%M:%S:%i  - %p - [%s]: %t";
	
	void configure_Logging();
	void print_startup_banner();

}

#endif // CH_LOGGING_CONFIGURATION_H
