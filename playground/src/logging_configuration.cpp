#include "logging_configuration.h"

namespace playground 
{
	void configure_Logging()
	{
		AutoPtr<ConsoleChannel> console_channel(new ConsoleChannel());
		AutoPtr<PatternFormatter> pattern_formatter(new PatternFormatter());
		pattern_formatter->setProperty("pattern", k_formatting_pattern);
		AutoPtr<FormattingChannel> formatting_channel(new FormattingChannel(pattern_formatter, console_channel));
		Logger::root().setChannel(formatting_channel);
		Logger::root().setLevel(k_log_level);
	}

	void print_startup_banner()
	{
		MODLOGGER.information("** Checkers Playground started ** ");
	}
}

