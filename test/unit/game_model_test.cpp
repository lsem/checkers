#include <gtest/gtest.h>

#include <Poco/StringTokenizer.h>
#include <Poco/String.h>

#include  "game_model_test.h"

using namespace checkers;
using namespace checkers::testing;

typedef std::auto_ptr<ITesteeBoardModel> TestModelPtr;

TestModelPtr create_testee_model(unsigned rows, unsigned columns)
{
    return TestModelPtr(checkers::testing::ModelsFactory::instance().create(rows, columns));    
}


TEST(CheckersTests_GameModel, board_construction_test)
{
    static const unsigned ROWS = 8;
    static const unsigned COLUMNS = 8; 
    TestModelPtr testee_model = create_testee_model(ROWS, COLUMNS);
    for (unsigned i = 0; i < ROWS; ++i)
    {
        for (unsigned j = 0; j < COLUMNS; ++j)
        {
            //std::printf("(%d, %d)\n", i, j);
            if ((i + j) % 2 == 0)
            {
                ASSERT_TRUE(testee_model->is_black_cell(i, j));
            }
            else
            {
                ASSERT_TRUE(testee_model->is_white_cell(i, j));
            }
        }
    }
}

const char * k_new_game_board =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- b - b - b - b" CHD_EOF
    /*6*/"b - b - b - b -" CHD_EOF
    /*5*/"- b - b - b - b" CHD_EOF
    /*4*/"- - - - - - - -" CHD_EOF
    /*3*/"- - - - - - - -" CHD_EOF
    /*2*/"w - w - w - w -" CHD_EOF
    /*1*/"- w - w - w - w" CHD_EOF
    /*0*/"w - w - w - w -"; 
TEST(CheckersTests_GameModel, board_reset_test)
{
    auto model = create_testee_model(8, 8);
    const auto etalon_board_model = SimpleModelDeserizlier::deserialize_model(k_new_game_board);
    model->reset();
    ASSERT_TRUE(TestModelsHelpers::are_models_equal(*model, *etalon_board_model));
    model->remove_figure(2, 2);
    // now after removing 2,2 model must be not the same as clean board
    ASSERT_FALSE(TestModelsHelpers::are_models_equal(*model, *etalon_board_model));
    // make reset and check if after everything is ok again
    model->reset();
    ASSERT_TRUE(TestModelsHelpers::are_models_equal(*model, *etalon_board_model));

    EXPECT_EQ(12, model->blacks_figures_count());
    EXPECT_EQ(12, model->white_figures_count());    
}

// -------------------------------
// Test Board:
const char * k_test_board_001 =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- b - b - b - b" CHD_EOF
    /*6*/"b - b - b - b -" CHD_EOF
    /*5*/"- b - b - b - b" CHD_EOF
    /*4*/"- - - - - - - -" CHD_EOF
    /*3*/"- - - - - - - -" CHD_EOF
    /*2*/"w - w - w - w -" CHD_EOF
    /*1*/"- w - w - w - w" CHD_EOF
    /*0*/"w - w - w - w -"; 
// -------------------------------
// Test description:
//  We expect that on given board for 2,0 there are next allowed moves:
//    3,1 (move)
//
//  While for 5,1:
//    4,0 (move)
//    4,2 (move)
// -------------------------------
TEST_F(BoardModelTest, basic_moves_test)
{
    on_board(k_test_board_001);
    EXPECT_TRUE(
        move(2,0,  3,1, TypeMove)
         .and(5,1,  4,0, TypeMove)
         .and(5,1,  4,2, TypeMove)
       .must_be_ONLY_allowed()
    ) << failure_message();

    // As test 1 but also ensure they cannot move further 
    EXPECT_TRUE(
        move(2,0,  4,2, TypeMove)
        .and(5,1,  3,-1, TypeMove)
        .and(5,1,  3,3, TypeMove)
        .must_NOT_be_allowed()
        ) << failure_message();

    // Ensure figures cannot move to occupied position
    EXPECT_TRUE(
        move(1,3,  1,3, TypeMove)
        .and(1,3,  2,2, TypeMove)
        .and(1,3,  2,4, TypeMove)
        .must_NOT_be_allowed()
        ) << failure_message();


    EXPECT_FALSE( // must_be_ONLY_allowed() selftest (Not only allowed moves (also [5,1] -> [4,2] must be))
        move(2,0,  3,1, TypeMove)
        .and(5,1,  4,0, TypeMove)        
        .must_be_ONLY_allowed()
        ) << failure_message();

    EXPECT_TRUE( // must_be_allowed() self tst ( Not only allowed moves (also [5,1] -> [4,2] must be))
        move(2,0,  3,1, TypeMove)
        .and(5,1,  4,0, TypeMove)        
        .must_be_allowed()  // as previous one, but without ONLY (means that given moves must be allowed but 
                            // besides them another ones can be allowed too)
        ) << failure_message();
    EXPECT_TRUE(
        move(2,0,  2,1, TypeMove)
         .and(2,0,  1,0, TypeMove)
         .and(2,0,  1,1, TypeMove)        
         .and(2,0,  3,0, TypeMove)        
         .and(2,0,  4,2, TypeMove)        
       .must_NOT_be_allowed()
    ) << failure_message();
}


// -------------------------------
// Test Board:
const char * k_test_board_002 =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- b - b - b - b" CHD_EOF
    /*6*/"b - b - b - b -" CHD_EOF
    /*5*/"- b - b - b - -" CHD_EOF
    /*4*/"- - - - - - - -" CHD_EOF
    /*3*/"- - - - - b - -" CHD_EOF
    /*2*/"w - w - w - w -" CHD_EOF
    /*1*/"- w - w - w - w" CHD_EOF
    /*0*/"w - w - w - w -"; 
// -------------------------------
// Test description:
//  We expect that on given board for 2,0 there are next allowed moves:
//    3,1 (move)
//
//  While for 5,1:
//    4,0 (move)
//    4,2 (move)
// -------------------------------
TEST_F(BoardModelTest, basic_attacks_test)
{
    on_board(k_test_board_002);
    // Test disallowed attacks
    EXPECT_TRUE(
        move(1,3,  3,5, TypeAttack)
        .must_NOT_be_allowed()
    ) << failure_message();

    EXPECT_TRUE(
        move(2,4,  4,6, TypeAttack)
        .must_be_ONLY_allowed()
    ) << failure_message("Figure must attack if there is enemy figure under attack");    

    EXPECT_TRUE(
        move(2,6,  4,4, TypeAttack)
        .must_be_allowed()
    ) << failure_message();
}
// -------------------------------
// Test description:
//  Test if board returns right moves for cases when some figure 
//  must be allowed to attack in different directions.
//    
// -------------------------------
// Test Board:
const char * k_test_board_002_a =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- - - b - b - b" CHD_EOF
    /*6*/"- - - - b - b -" CHD_EOF
    /*5*/"- b - - - b - -" CHD_EOF
    /*4*/"- - w - - - - -" CHD_EOF
    /*3*/"- b - b - b - -" CHD_EOF
    /*2*/"w - - - - - w -" CHD_EOF
    /*1*/"- w - w - w - w" CHD_EOF
    /*0*/"w - w - w - w -"; 


TEST_F(BoardModelTest, more_sophisticated_attacks_test)
{
    on_board(k_test_board_002_a);
    EXPECT_TRUE(
        move(4,2,  6,0, TypeAttack)
        .and(4,2,  2,4, TypeAttack)
        .must_be_ONLY_allowed()
    ) << failure_message();
}

// -------------------------------
// Test Board:
static const char * k_test_board_003 =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- - - - - - - -" CHD_EOF
    /*6*/"- - - - - - - -" CHD_EOF
    /*5*/"- - - - - - - -" CHD_EOF
    /*4*/"- - - - b - - -" CHD_EOF
    /*3*/"- - - - - - - -" CHD_EOF
    /*2*/"- - - w - - - -" CHD_EOF
    /*1*/"- - - - - - - -" CHD_EOF
    /*0*/"- - - - - - - -"; // -------------------------------
// Test description:
//    Test if figures cannot move backward.
// On given board 2,3(w) cannot move backward and 4,4 cannot too.
// -------------------------------
TEST_F(BoardModelTest, moves_backward_dissalowed_test)
{
    on_board(k_test_board_003);    
    EXPECT_TRUE(
        move(2,3,  2,3, TypeMove) // cannot make move without chaning position
         .and(2,3,  1,3, TypeMove)
         .and(2,3,  1,2, TypeMove)
         .and(2,3,  1,4, TypeMove)
         .and(2,3,  2,4, TypeMove)
         .and(2,3,  2,2, TypeMove)
         .and(4,4,  4,4, TypeMove)
         .and(4,4,  4,5, TypeMove)
         .and(4,4,  5,5, TypeMove)
         .and(4,4,  5,3, TypeMove)
        .must_NOT_be_allowed()
    ) << failure_message();
}

// -------------------------------
// Test Board:
static const char * k_test_board_005 =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- - - - - - - -" CHD_EOF
    /*6*/"w - - - - - - -" CHD_EOF
    /*5*/"- b - - - - - -" CHD_EOF
    /*4*/"- - - - w - - -" CHD_EOF
    /*3*/"- - - b - - - -" CHD_EOF
    /*2*/"- - - - - - - -" CHD_EOF
    /*1*/"- - - - - - - -" CHD_EOF
    /*0*/"- - - - - - - -"; 
// -------------------------------
// Test description:
//    Test if board behave fine in backwards moves
// -------------------------------
TEST_F(BoardModelTest, backward_attack_test)
{
    on_board(k_test_board_005);
    ASSERT_TRUE(
        move(3,3, 5,5, TypeAttack)
         .and(4,4, 2,2, TypeAttack)
         .must_be_ONLY_allowed()
    ) << failure_message();

    ASSERT_TRUE(
        move(6,0, 4,2, TypeAttack)
        .must_be_ONLY_allowed()
    ) << failure_message();
}

// -------------------------------
// Test Board:
static const char * k_test_board_006 =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- - - - - - - -" CHD_EOF
    /*6*/"- - - - - - - -" CHD_EOF
    /*5*/"- - - - - - - -" CHD_EOF
    /*4*/"- - - - - - - -" CHD_EOF
    /*3*/"- - - w - - - -" CHD_EOF
    /*2*/"- - - - - - - -" CHD_EOF
    /*1*/"- - - - - - - -" CHD_EOF
    /*0*/"- - - - - - - -"; 
// -------------------------------
// Test description:
//    Test if board behave fine in attack mode. Attack mode
//     means that only attack moves are allowed and if there 
//     is only motion moves they must be disallowed anyway

// -------------------------------
TEST_F(BoardModelTest, attack_mode_test)
{
    on_board(k_test_board_006);
    ASSERT_TRUE(
         attack_mode_enabled()
        .and_no_moves_allowed_from(3, 3)
    );
      

    // Test if in disabled attack mode they still can move (forward only)
    ASSERT_TRUE(
        attack_mode_disabled()
        .and()
        .move(3,3, 4,2, TypeMove)
        .and(3,3, 4,4, TypeMove)
        .must_be_ONLY_allowed()
    ) << failure_message();
}

// -------------------------------
// Test Board:
static const char * k_test_board_007 =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- b - b - - - b" CHD_EOF
    /*6*/"b - b - b - - -" CHD_EOF
    /*5*/"- b - b - - - -" CHD_EOF
    /*4*/"- - - - - - - w" CHD_EOF
    /*3*/"- - - - - - - -" CHD_EOF
    /*2*/"w - w - - - - -" CHD_EOF
    /*1*/"- w - w - b - -" CHD_EOF
    /*0*/"w - w - - - - -"; 
// -------------------------------
// Test description:
//    Test for different moves cases closely to boards boundary.
//  ------------------------------
TEST_F(BoardModelTest, board_bondary_moves_test)
{
    on_board(k_test_board_007);
    EXPECT_TRUE(
        move(4,7, 5,6,  TypeMove)
        .must_be_ONLY_allowed()
    ) << failure_message();
    
    EXPECT_TRUE(no_moves_allowed_from(6, 0));    
    
    EXPECT_TRUE(no_moves_allowed_from(7, 1));
    
    EXPECT_TRUE(move(7,7,  6,6, TypeMove).must_be_ONLY_allowed()) << failure_message();    

    EXPECT_TRUE(
        move(1,5,  0,4, TypeMove)
        .and(1,5,  0,6, TypeMove)
      .must_be_ONLY_allowed()
    ) << failure_message();    
}

// -------------------------------
// Test Board:
static const char * k_test_board_008_source =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- - - - - - - b" CHD_EOF
    /*6*/"- - - b - - - -" CHD_EOF
    /*5*/"- - w - - - - -" CHD_EOF
    /*4*/"- - - - - - - w" CHD_EOF
    /*3*/"- - - - - - - -" CHD_EOF
    /*2*/"- - - - - - - -" CHD_EOF
    /*1*/"- - - - - b - -" CHD_EOF
    /*0*/"- - - - - - - -"; 
static const char * k_test_board_008_expected_after =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- - - - w - - -" CHD_EOF
    /*6*/"- - - - - - - -" CHD_EOF
    /*5*/"- - - - - - w b" CHD_EOF
    /*4*/"- - - - - - - -" CHD_EOF
    /*3*/"- - - - - - - -" CHD_EOF
    /*2*/"- - - - - - - -" CHD_EOF
    /*1*/"- - - - - - - -" CHD_EOF
    /*0*/"- - - - b - - -"; 
// -------------------------------
// Test description:
//    Test if by asking board to move some figures 
//    it really does what we want. 
//  ------------------------------
TEST_F(BoardModelTest, basic_move_actions_tests)
{
    on_board(k_test_board_008_source);
    unsigned bc = model().blacks_figures_count();
    unsigned wc = model().white_figures_count();
    // do moves
    model().make_move(1,5, 0,4);
    model().make_move(4,7, 5,6);
    model().make_move(7,7, 6,6);
    model().make_move(6,6, 5,7);
    // TODO: move attack checks to dedicated test
    model().make_attack(5,2, 7,4, 6,3);
    EXPECT_EQ(model().blacks_figures_count(), (bc - 1));    
    // check if result board is as we expect
    auto model_expected = SimpleModelDeserizlier::deserialize_model(k_test_board_008_expected_after);    
    EXPECT_TRUE(TestModelsHelpers::are_models_equal(model(), *model_expected));
    
}



