#include "test_models_helpers.h"


namespace checkers 
{
    namespace testing 
    {        
        bool TestModelsHelpers::are_models_equal(const ITesteeBoardModel& modelA, const ITesteeBoardModel& modelB)
        {
            bool are_different = false;
            bool size_ok = modelA.rows_count() == modelB.rows_count()  &&
                            modelA.columns_count() == modelB.columns_count();
            if (size_ok)
            {
                unsigned rows = modelA.rows_count();
                unsigned cols = modelA.columns_count();
                for (unsigned i = 0; !are_different && i < rows; ++i)
                {
                    for (unsigned j = 0; !are_different && j < cols; ++j)
                    {                   
                        are_different = modelA.is_black_cell(i, j) != modelB.is_black_cell(i, j) || 
                                        modelA.is_white_cell(i, j) != modelB.is_white_cell(i, j) || 
                                        modelA.is_queen(i, j)      != modelB.is_queen(i, j)      || 
                                        modelA.is_black_cell(i, j) != modelB.is_black_cell(i, j) || 
                                        modelA.is_black_cell(i, j) != modelB.is_black_cell(i, j) || 
                                        modelA.is_white_cell(i, j) != modelB.is_white_cell(i, j) || 
                                        modelA.is_empty_cell(i, j) != modelB.is_empty_cell(i, j);
                    }
                }

            }
            return !are_different;
        }

    }
}
