#ifndef CH_MODEL_DESERIAZLIER_H
#define CH_MODEL_DESERIAZLIER_H

#include <string>
#include <map>
#include <memory>

#include "test_model_intf.h"

// Checkers deserializer EOF token
#define CHD_EOF "\n"
#define CHD_HDR_8X8_CLEAN_BOARD "{r:8,c:8,b:+,w:-},"CHD_EOF
#define CHD_HDR_8X8_DEFAULT_AUTO_BOARD "{r:8,c:8,b:-,w:-},"CHD_EOF


static const char * k_board_8x8_template =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- - - - - - - -" CHD_EOF
    /*6*/"- - - - - - - -" CHD_EOF
    /*5*/"- - - - - - - -" CHD_EOF
    /*4*/"- - - - - - - -" CHD_EOF
    /*3*/"- - - - - - - -" CHD_EOF
    /*2*/"- - - - - - - -" CHD_EOF
    /*1*/"- - - - - - - -" CHD_EOF
    /*0*/"- - - - - - - -"; 

namespace checkers 
{
    namespace testing 
    {        
        class SimpleModelDeserizlier
        {
        public:
            typedef std::auto_ptr<ITesteeBoardModel> ModelAutoPtrType;

            enum ModelParam
            {
                ModelParam_Invalid = 0,
                ModelParam_RowsCount,
                ModelParam_ColumnsCount,
                ModelParam_BlackCellToken,
                ModelParam_WhiteCellToken
            };

            struct BoardConfiguration
            {        
                std::string bctok; // black cell token
                std::string wctok; // white cell token
                std::string bftok; // black figure token
                std::string wftok; // white figure token
                std::string bftok_q; // black queen figure token
                std::string wftok_q; // white queen figure token
                unsigned rows_count;
                unsigned columns_count;
            };


            typedef std::map<ModelParam, std::string> DictionaryType;

            typedef std::pair<size_t, DictionaryType> ModelInfoType;

            static BoardConfiguration get_model_configuration(const ModelInfoType& header_info);

            static bool is_valid_model(const ModelInfoType& minfo) 
            {
                return minfo.first > 0 && minfo.first != std::string::npos;
            }

            static ModelParam decode_model_param(const std::string& strencoded_val);
            static ModelInfoType parse_header(const std::string& model_text_data);
            
            // Deserializes model and return (smart) pointer to it. In case any error std::runtime_error is raised.
            // Therefore returned model must be valid model object ready for testing.
            static ModelAutoPtrType deserialize_model(const std::string& model_text_data);
        };



    }
}


#endif // CH_MODEL_DESERIAZLIER_H


