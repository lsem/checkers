#ifndef CH_TEST_MODELS_HELPERS_H
#define CH_TEST_MODELS_HELPERS_H
#include <list>
#include <sstream>
#include <iostream>

#include "test_model_intf.h"

namespace checkers 
{
    namespace testing 
    {        
        class TestModelsHelpers 
        {
        public:
            // Checks if boards are equal (cells at arbitrary i,j has the same color and the same figures)
            static bool are_models_equal(const ITesteeBoardModel& modelA, const ITesteeBoardModel& modelB);

        };
    }
}


#endif // CH_TEST_MODELS_HELPERS_H




