#ifndef CH_TEST_MODEL_INTF_H
#define CH_TEST_MODEL_INTF_H
#include <list>

namespace checkers 
{
    namespace testing
    {

        // Interface for test models. If you want to run tests on your custom model with similar functionality
        // you need to 'adapt' your class to this interface. 
        // Please see http://en.wikipedia.org/wiki/Adapter_pattern for details.
        class ITesteeBoardModel
        {
        public:
            struct Move
            {                
                enum Type 
                { 
                    Type_BEGIN = 0, 
                    Type_Invalid = Type_BEGIN, 
                    Type_Move = 1, 
                    Type_Attack = 2,
                    Type_END
                };
                Move() {};
                Move(unsigned row, unsigned col, Type type) : kind(type), to_row(row), to_col(col) {}   
                Type kind;
                unsigned to_row;
                unsigned to_col;
            };
        public:
            typedef unsigned int ui_t;                        
            typedef std::list<Move> MovesCollection;
            typedef int PlayerIDType;

            virtual ~ITesteeBoardModel() {}

            virtual unsigned blacks_figures_count() const = 0;            
            
            virtual unsigned white_figures_count() const = 0;

            // Returns of rows count of the board so that (rows_count() - 1) must be coordiate of last row of the board.
            virtual unsigned rows_count() const = 0;
            // Returns of rows count of the board so that (columns_count() - 1) must be coordiate of last column of the board.
            virtual unsigned columns_count() const = 0;
            // Make cell white whatever is put on (becomes white empty).
            virtual void set_white_cell(ui_t row, ui_t col) = 0;
            // Make cell black whatever is put on (becomes black empty).
            virtual void set_black_cell(ui_t row, ui_t col) = 0;
            // Set white figure whatever is figure is put (preserves cell color).
            virtual void put_white_fig(ui_t row, ui_t col) = 0;
            // Set black figure whatever is figure is put (preserves cell color).
            virtual void put_black_fig(ui_t row, ui_t col) = 0;            
            // Make current figure queen.
            virtual void make_queen(ui_t row, ui_t col) = 0;            
            // Remove figure from specified position. If there is no any figure then do nothing.
            virtual void remove_figure(ui_t row, ui_t col) = 0;
            // Reset model state so that is must be ready for new game.
            virtual void reset() = 0;
            // Returns if specified cell is queen
            virtual bool is_queen(ui_t row, ui_t col) const = 0;
            // Returns if specified cell is white cell.
            virtual bool is_white_cell(ui_t row, ui_t col) const = 0;
            // Returns if specified cell is black.
            virtual bool is_black_cell(ui_t row, ui_t col) const = 0;
            // Returns of specified cell is white figure (owner is 1-st player)
            virtual bool is_white_fig(ui_t row, ui_t col) const = 0;
            // Returns if specified cell is black figure (owner is 1-st player)
            virtual bool is_black_fig(ui_t row, ui_t col) const = 0;
            // Returns if last move was attack and thus only attack moves are allowed (also knows about player).
            virtual bool is_attack_mode() const = 0;
            // Returns true is cell is empty (either black or white but no figures placed there).
            virtual bool is_empty_cell(ui_t row, ui_t col) const = 0;

            // Generates possible moves in given board state.
            virtual void generate_available_moves(ui_t row, ui_t col, bool attack_modde, MovesCollection& out_moves) const = 0;
                        
            virtual bool is_move_allowed(ui_t row_from, ui_t col_from, ui_t row_to, ui_t col_to, Move::Type type, bool attack_mode = false) const = 0;
            
            virtual void make_move(ui_t row_from, ui_t col_from, ui_t row_to, ui_t col_to, bool attack_mode = false) = 0;
            virtual void make_attack(ui_t row_from, ui_t col_from, ui_t row_to, ui_t col_to, ui_t attacked_row, ui_t attacked_col, bool attack_mode = false)  = 0;

        };

    }
}

#endif // CH_TEST_MODEL_INTF_H
