#ifndef CH_MODELS_FACTORY_H
#define CH_MODELS_FACTORY_H
#include <memory>
#include "test_model_intf.h"

namespace checkers 
{
    namespace testing
    {
        template <class ModelBaseType>
        class GenericModelsFactory
        {
        public:            
            typedef ModelBaseType* BasePtr;
                        
            template <class DerivedModel>
            void specialize()
            {
                _creator = BaseTypePtr(new DerivedType<DerivedModel>());
            }

            BasePtr create(unsigned rows, unsigned columns) const
            {
                return _creator->create(rows, columns);
            }

            static GenericModelsFactory& instance() 
            {
                static GenericModelsFactory<ModelBaseType>* instance_ptr = new GenericModelsFactory<ModelBaseType>();
                return *instance_ptr;
            }

        private:
            
            GenericModelsFactory()
            {
            }

            class BaseType
            {
            public:
                virtual ~BaseType() {}
                virtual BasePtr create(unsigned rows, unsigned columns) const = 0;
            };

            typedef std::shared_ptr<BaseType> BaseTypePtr;

            template <class T>
            class DerivedType : public BaseType
            {
            public:
                virtual BasePtr create(unsigned rows, unsigned columns) const
                {
                    return BasePtr(new T(rows, columns));
                }
            };
            
             BaseTypePtr _creator;
        };

        typedef GenericModelsFactory<ITesteeBoardModel> ModelsFactory;
    }
}



#endif // CH_MODELS_FACTORY_H
