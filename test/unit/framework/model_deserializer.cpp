#include <cassert>
#include <iostream>

#include <Poco/String.h>
#include <Poco/StringTokenizer.h>

#include <unit/framework/model_deserializer.h>
#include <unit/framework/models_factory.h>

namespace checkers 
{
    namespace testing 
    {

        SimpleModelDeserizlier::ModelParam SimpleModelDeserizlier::decode_model_param(const std::string& strencoded_val)
        {       
            ModelParam result = ModelParam_Invalid;
            // TODO: add trimming
            if (Poco::icompare(strencoded_val, "r") == 0)
            {
                result = ModelParam_RowsCount;
            }
            else if (Poco::icompare(strencoded_val, "c") == 0)
            {
                result = ModelParam_ColumnsCount;
            }
            else if (Poco::icompare(strencoded_val, "b") == 0)
            {
                result = ModelParam_BlackCellToken;
            }
            else if (Poco::icompare(strencoded_val, "w") == 0)
            {
                result = ModelParam_WhiteCellToken;
            }
            return result;
        }

        // Returns a pair of <dictionary, pos> where pos is position of data start, or std::string::npos if header is invalid.
        SimpleModelDeserizlier::ModelInfoType SimpleModelDeserizlier::parse_header(const std::string& model_text_data)
        {
            ModelInfoType result;        
            ModelInfoType::second_type& dictionary = result.second;
            size_t header_start = model_text_data.find_first_not_of(" \t\n\r");   
            if (header_start != std::string::npos && (model_text_data.at(header_start) == '{'))
            {
                size_t header_end = model_text_data.find("}", header_start);
                if (header_end != std::string::npos)
                {                                
                    size_t datastart_token_pos = model_text_data.find(",", header_end);
                    result.first = datastart_token_pos + 1;
                    if (datastart_token_pos != std::string::npos)
                    {                    
                        // header boundaries found
                        std::string header_data(model_text_data, header_start + 1, header_end - header_start - 1);
                        Poco::StringTokenizer tokenizer(header_data, ","); 

                        for (Poco::StringTokenizer::Iterator it = tokenizer.begin(); it != tokenizer.end(); ++it)
                        {
                            size_t delim_pos = it->find(":");
                            if (delim_pos != std::string::npos)
                            {
                                std::string keystr = it->substr(0, delim_pos);
                                ModelParam key = decode_model_param(keystr);
                                if (key != ModelParam_Invalid)
                                {
                                    std::string val = it->substr(delim_pos + 1);
                                    dictionary[key] = val;
                                }                                                        
                                //std::cout << "(key, val)= " << "("<< key << ", " << val << ")\n";
                            }
                        }
                    }
                }
            }
            if (dictionary.find(ModelParam_RowsCount) == dictionary.end() 
                || dictionary.find(ModelParam_ColumnsCount) == dictionary.end())
            {
                std::cerr << "error: failed to parse serialized model. Mandatory parameters are missing (r, c)\n";
                result.first = std::string::npos;            
            }
            return result;
        }

        SimpleModelDeserizlier::BoardConfiguration SimpleModelDeserizlier::get_model_configuration(const ModelInfoType& header_info)
        {
            BoardConfiguration result;
            result.bctok ="-";
            result.wctok = "-";
            result.bftok= "b";
            result.wftok= "w";
            result.bftok_q= "B";
            result.wftok_q= "W";
            for (DictionaryType::const_iterator it = header_info.second.begin();
                it != header_info.second.end(); ++it)
            {                
                switch(it->first)
                {
                case ModelParam_ColumnsCount:
                    {
                        result.columns_count= atoi(it->second.c_str());
                        break;
                    }
                case ModelParam_RowsCount:
                    {
                        result.rows_count = atoi(it->second.c_str());
                        break;
                    }
                case ModelParam_BlackCellToken:
                    {
                        result.bctok = it->second;
                        break;
                    }
                case ModelParam_WhiteCellToken:
                    {
                        result.wctok = it->second;
                        break;
                    }
                }
            }
            return result;
        }
        
        SimpleModelDeserizlier::ModelAutoPtrType SimpleModelDeserizlier::deserialize_model(const std::string& model_text_data)
        {
            ModelAutoPtrType result;
            ModelInfoType parsehead_res = parse_header(model_text_data);
            if (is_valid_model(parsehead_res))
            {            
                BoardConfiguration bconf = get_model_configuration(parsehead_res);
                bool celltok_auto = bconf.bctok == bconf.wctok;
                size_t data_start = parsehead_res.first;
                std::string model_data(model_text_data, data_start);
                Poco::StringTokenizer tokenizer(model_data, " \n", Poco::StringTokenizer::TOK_IGNORE_EMPTY);            
                if (tokenizer.count() == bconf.rows_count* bconf.columns_count)
                {                                    
                    result.reset(ModelsFactory::instance().create(bconf.rows_count, bconf.columns_count));
                    for (unsigned i = 0; i < bconf.rows_count; ++i)
                    {
                        for (unsigned j = 0; j < bconf.columns_count; ++j)
                        {                            
                            unsigned serialized_i = bconf.rows_count - i - 1;
                            unsigned serialized_j = j;
                            size_t offset = serialized_i * bconf.columns_count + serialized_j;
                            assert(offset < tokenizer.count());
                            // translate coordinates from serialized data (0,0 at left top) to 
                            // our model ( 0,0 at left bottom)
                            Poco::StringTokenizer::Iterator offs_it = tokenizer.begin();
                            std::advance(offs_it, offset);
                            std::string tok = *offs_it; 
                            // set cell (white, black)
                            bool black_token = celltok_auto && (i + j) % 2 == 0 
                                || !celltok_auto && (tok == bconf.bctok);
                            if (black_token)
                            {
                                result->set_black_cell(i, j);
                            }
                            else
                            {
                                result->set_white_cell(i, j);
                            }
                            // place figure
                            if (tok == bconf.bftok)                            
                            {
                                result->put_black_fig(i, j);
                            }
                            else if (tok == bconf.wftok)
                            {
                                result->put_white_fig(i, j);
                            }
                            else if (tok == bconf.bftok_q)
                            {
                                result->put_black_fig(i, j);
                                result->make_queen(i, j);
                            }
                            else if (tok == bconf.wftok_q)
                            {
                                result->put_white_fig(i, j);
                                result->make_queen(i, j);
                            }
                        }
                    }                
                }
                else
                {
                    std::runtime_error("Invalid model specified: declared board size is not accurate");
                }
            }
            if (!result.get())
            {
                throw std::runtime_error("cannot deserialize model data: " + std::string(model_text_data));
            }
            return result;
        }


    }
}
