#ifndef CH_BOARD_MODEL_TEST_H
#define CH_BOARD_MODEL_TEST_H

#include <iostream>
#include <map>

#include <gtest/gtest.h>

#include <unit/framework/models_factory.h>
#include <unit/framework/model_deserializer.h>
#include <unit/framework/test_models_helpers.h>

#include <common/board_position.h>

using namespace checkers;
using namespace checkers::testing;


class BoardModelTest : public ::testing::Test 
{
public:

    enum MoveTypeAlias
    {
        TypeMove = ITesteeBoardModel::Move::Type_Move,
        TypeAttack = ITesteeBoardModel::Move::Type_Attack,
        TypeInvalid = ITesteeBoardModel::Move::Type_Invalid,
        TypeAnyMove = TypeInvalid + 1
    };

    void SetUp()
    {
        _attack_mode = false;
        _moves.clear();        
    }

    ITesteeBoardModel& model()
    {
        assert(_model.get() != NULL);
        return *_model;
    }

    void on_board(const char* serialized_board_model)
    {
        _model = SimpleModelDeserizlier::deserialize_model(serialized_board_model);
    }

    BoardModelTest& move(int fr, int fc, int tr, int tc, MoveTypeAlias kind)
    {
        _attack_mode = false;
        _moves.clear();
        _moves.push_back(MoveEntry(BoardPos(fr, fc), BoardPos(tr, tc), (ITesteeBoardModel::Move::Type) kind));
        return *this;
    }

    BoardModelTest& and(int fr, int fc, int tr, int tc, MoveTypeAlias kind)
    {
        _moves.push_back(MoveEntry(BoardPos(fr, fc), BoardPos(tr, tc), (ITesteeBoardModel::Move::Type)kind));
        return *this;
    }

    BoardModelTest& and()
    {
        return *this;
    }

    // Explicitly disable attack mode
    BoardModelTest& attack_mode_disabled()
    {
        _attack_mode = false;
        return *this;
    }

    BoardModelTest& attack_mode_enabled()
    {
        return and_attack_mode_enabled();
    }

    BoardModelTest& and_attack_mode_enabled()
    {
        _attack_mode = true;
        return *this;
    }

    bool must_be_allowed()
    {        
        return moves_allowance_test(/*expected_allowed=*/true, /*only=*/false);
    }

    // Returns true if given moves are only allowed moves (but not for whole board but only for source position,
    // i.e if pair (x1,y1) -> (x2, y2, M) is in given moves, (x1, y1) -> x_I,h_J,mT must be disallowed for any I,J,T 
    // in (0..model->rows, 0..model->columns, MoveType_MIN, MoveType_MAX))
    bool must_be_ONLY_allowed()
    {
        return moves_allowance_test(/*expected_allowed=*/true, /*only=*/true);
    }

    bool and_no_moves_allowed_from(int fr, int fc) const
    {
        return no_moves_allowed_from(fr, fc);
    }

    bool no_moves_allowed_from(int fr, int fc) const
    {
        ITesteeBoardModel::MovesCollection available_moves;
        _model->generate_available_moves(fr, fc, _attack_mode, available_moves); 
        return available_moves.empty();
    }

    bool must_NOT_be_allowed()
    {
        return moves_allowance_test(/*expected_allowed=*/false, /*only=*/false);
    }
    
    std::string failure_message(const std::string additional_message = std::string()) const 
    {
        return additional_message + "\n" + _last_failure_msg;
    }

private:
    // Flag only indicates that except given moves all others must be NOT expected_allowed.
    bool moves_allowance_test(bool expected_allowed, bool only)
    {
        bool result = false;
        verify_test_valid();
        
        bool cond_true = true;
        MovesCollection::const_iterator mit;
        
        typedef std::map<BoardPos, std::set<MoveEntry> > TestedMovesMap;
        TestedMovesMap tested_moves;// mapping from positions to moves 

        for (mit = _moves.begin(); cond_true && mit != _moves.end(); ++mit)
        {
            bool is_allowed  = _model->is_move_allowed(mit->from.row(), mit->from.column(), 
                mit->to.row(), mit->to.column(), mit->kind);
            cond_true = (is_allowed == expected_allowed); // both allowed or both disallowed
            if (!cond_true)
            {
                format_test_failure_msg(*mit, !expected_allowed);
            }   
            tested_moves[mit->from].insert(*mit);
        }

        if (cond_true && only)
        {
            for (TestedMovesMap::const_iterator fmit=tested_moves.begin(); cond_true && fmit != tested_moves.end(); ++fmit)
            {
                // check if moves in ->second are only allowed/disallowed moves
                //for (std::set<MoveEntry>::const_iterator tit = fmit->second.begin(); cond_true && tit != fmit->second.end(); ++tit)
                {
                    for (unsigned to_i = 0; cond_true && to_i < _model->rows_count(); ++to_i)
                    {
                        for (unsigned to_j = 0; cond_true && to_j < _model->columns_count(); ++to_j)
                        {
                            BoardPos target_pos = BoardPos(to_i, to_j);
                            const BoardPos& source_pos = fmit->first;
                            if (source_pos != target_pos )
                            {
                                for (unsigned kind = ITesteeBoardModel::Move::Type_BEGIN; cond_true && kind != ITesteeBoardModel::Move::Type_END; ++kind)
                                {
                                    MoveEntry target_move(source_pos, target_pos, (ITesteeBoardModel::Move::Type)kind);
                                    if (fmit->second.find(target_move) == fmit->second.end())
                                    {
                                        bool actual_is_allowed = _model->is_move_allowed(source_pos.row(), source_pos.column(),
                                            target_pos.row(), target_pos.column(), (ITesteeBoardModel::Move::Type)kind);
                                        cond_true = (actual_is_allowed != expected_allowed);
                                        if (!cond_true)
                                        {
                                            MoveEntry failure_test_move(source_pos, BoardPos(to_i, to_j), (ITesteeBoardModel::Move::Type)kind);
                                            format_test_failure_msg(failure_test_move, /*disallowed=*/expected_allowed);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        result = cond_true;
        return result;
    }


private:
    void verify_test_valid()
    {
        if (!_model.get())
        {
            throw std::logic_error("Missing model (hint: did you forget call on_board() in your test?)");
        }
    }


    static std::string print_board_model(const ITesteeBoardModel& board) 
    {
        std::stringstream ss;
        for (int i = board.rows_count() ; i >= 0 ; --i)
        {            
            ss << std::endl;                        
            if (i > 0)                       
                ss << i - 1 << ":  ";
            else
                ss <<  "    ";            
            for (unsigned j = 0; j < board.columns_count(); ++j)
            {
                if (i == 0)
                {
                    ss << j << " ";
                }
                else
                {           
                    unsigned row = i - 1; 
                    if (board.is_white_fig(row, j))
                    {
                        ss << (board.is_queen(row,j)? "W" : "w");                            
                    }
                    else if (board.is_black_fig(row, j))
                    {
                        ss << (board.is_queen(row, j)? "B" : "b"); 
                    }
                    else
                    {
                        ss << "-";
                    }
                    ss << " ";
                }
            }                                                    
        }
        return ss.str();
    }


private:    

    struct MoveEntry {
        MoveEntry() {}
        MoveEntry(const BoardPos& from, const BoardPos& to, ITesteeBoardModel::Move::Type kind)
            : from(from)
            , to(to)
            , kind(kind)
        {}
        bool operator<(const MoveEntry& other) const {
            if (from != other.from)
                return from < other.from;
            if (to != other.to)
                return to < other.to;
            if (kind != other.kind)
                return kind < other.kind;
            return false; // not less (equal or greater)
        }
        BoardPos from;
        BoardPos to;
        ITesteeBoardModel::Move::Type kind;
    };

    SimpleModelDeserizlier::ModelAutoPtrType _model;
    typedef std::list<MoveEntry> MovesCollection;
    MovesCollection _moves;
    bool _attack_mode;
    std::string _last_failure_msg;

private:
    
    void format_test_failure_msg(const MoveEntry& me, bool disallowed = false)
    {
        std::stringstream ss;
        ss << "On board: "  << print_board_model(*_model) << std::endl;
        ss << "The move from (" << me.from.row() << ", " << me.from.column() << ") to (" << 
            me.to.row() << ", " << me.to.column() << ") "
            "is expected to be " << (disallowed? "disallowed" : "allowed") << " while it is not." <<
            " Board model:\n";
        _last_failure_msg = ss.str();        
    }

};

#endif // CH_BOARD_MODEL_TEST_H

