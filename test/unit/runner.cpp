#include <gtest/gtest.h>
#include <unit/framework/models_factory.h>

#include <unit/framework/test_model_intf.h>
#include "checkers_model_adapater.h"

int main(int argc, char * argv[])
{
    // creating instance of factory and specialize for creating models BoardModel_LS
    // (teach factory create instances of my model)
    checkers::testing::ModelsFactory::instance().specialize<checkers::testing::BoardModel_LS>();     
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
