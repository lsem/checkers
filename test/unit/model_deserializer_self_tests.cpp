#include <gtest/gtest.h>

#include <unit/framework/model_deserializer.h>

using namespace checkers;
using namespace checkers::testing;


const char * clean_board_8x8_01 =     
    CHD_HDR_8X8_CLEAN_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"- + - + - + - +" CHD_EOF
    /*6*/"+ - + - + - + -" CHD_EOF
    /*5*/"- + - + - + - +" CHD_EOF
    /*4*/"+ - + - + - + -" CHD_EOF
    /*3*/"- + - + - + - +" CHD_EOF
    /*2*/"+ - + - + - + -" CHD_EOF
    /*1*/"- + - + - + - +" CHD_EOF
    /*0*/"+ - + - + - + -"; 


TEST(CheckersTests_GameModel, model_deserializer_self_test_01)
{
    auto model = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01);
    ASSERT_TRUE(model->is_black_cell(0, 0));
    ASSERT_TRUE(model->is_white_cell(0, 1));
    ASSERT_TRUE(model->is_black_cell(0, 2));
    ASSERT_TRUE(model->is_black_cell(1, 1));
    ASSERT_TRUE(model->is_white_cell(1, 0));

    ASSERT_TRUE(model->is_black_cell(7, 7));
    ASSERT_TRUE(model->is_black_cell(0, 0));
    ASSERT_TRUE(model->is_white_cell(0, 7));
    ASSERT_TRUE(model->is_white_cell(7, 0));
}

const char * board_8x8_auto_01 =     
    CHD_HDR_8X8_DEFAULT_AUTO_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"W - - - - - - w" CHD_EOF
    /*6*/"- - - - - - - -" CHD_EOF
    /*5*/"- - - - - - - -" CHD_EOF
    /*4*/"- - - - - - - -" CHD_EOF
    /*3*/"- - - - - - - -" CHD_EOF
    /*2*/"- - - - - - - -" CHD_EOF
    /*1*/"- - - - - - - -" CHD_EOF
    /*0*/"b - - - - - B -"; 
TEST(CheckersTests_GameModel, model_deserializer_self_test_02)
{
    auto model = SimpleModelDeserizlier::deserialize_model(board_8x8_auto_01);
    ASSERT_TRUE(model->is_black_cell(0, 0));
    ASSERT_TRUE(model->is_white_cell(0, 1));
    ASSERT_TRUE(model->is_black_cell(0, 2));
    ASSERT_TRUE(model->is_black_cell(1, 1));
    ASSERT_TRUE(model->is_white_cell(1, 0));

    ASSERT_TRUE(model->is_black_cell(7, 7));
    ASSERT_TRUE(model->is_black_cell(0, 0));
    ASSERT_TRUE(model->is_white_cell(0, 7));
    ASSERT_TRUE(model->is_white_cell(7, 0));
    
    // Test for empty cells
    for (unsigned i = 0; i < 8; ++i)
    {
        for (unsigned j = 0; j < 8; ++j)
        {
            if (! ((i == 0 && j == 0) ||
                   (i == 0 && j == 6) ||
                   (i == 7 && j == 7) ||
                   (i == 7 && j == 0)) ) // white queen must be there
            {
                ASSERT_TRUE(model->is_empty_cell(i, j));
                ASSERT_FALSE(model->is_black_fig(i, j))  << "Empty position cannot be black figure";
                ASSERT_FALSE(model->is_white_fig(i, j))  << "Empty position cannot be white figure";
                ASSERT_FALSE(model->is_queen(i, j)) << "Empty position cannot be queen";
            }
        }
    }
    // check if figures on right places
    ASSERT_TRUE(model->is_black_fig(0, 0));    
    ASSERT_TRUE(model->is_black_fig(0, 6));
    ASSERT_TRUE(model->is_queen(0, 6));
    ASSERT_TRUE(model->is_white_fig(7, 7));
    ASSERT_FALSE(model->is_queen(7, 7));
    ASSERT_TRUE(model->is_white_fig(7, 0));
    ASSERT_TRUE(model->is_queen(7, 0));
}
