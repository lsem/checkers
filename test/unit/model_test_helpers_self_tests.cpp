#include <gtest/gtest.h>

#include <unit/framework/model_deserializer.h>
#include <unit/framework/test_models_helpers.h>

using namespace checkers;
using namespace checkers::testing;


static const char * clean_board_8x8_01 =     
    CHD_HDR_8X8_CLEAN_BOARD
    /*    0 1 2 3 4 5 6 7 */
    /*7*/"b + - + - + - +" CHD_EOF
    /*6*/"+ - + - + - w -" CHD_EOF
    /*5*/"- + - + - + - +" CHD_EOF
    /*4*/"+ - + - + - + -" CHD_EOF
    /*3*/"- + - W - + - +" CHD_EOF
    /*2*/"+ - + - + - + -" CHD_EOF
    /*1*/"- + - + - + B +" CHD_EOF
    /*0*/"w - + - + - + b"; 
TEST(CheckersTests_GameModel, model_test_helpers_self_test_models_equal_test_01)
{
    auto modelA = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01);
    auto modelB = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01);
    ASSERT_TRUE(TestModelsHelpers::are_models_equal(*modelA, *modelB));

}

namespace test02
{
    // Test 02
    static const char * clean_board_8x8_01__b1 =     
        CHD_HDR_8X8_CLEAN_BOARD
        /*    0 1 2 3 4 5 6 7 */
        /*7*/"b + - + - + - +" CHD_EOF
        /*6*/"+ - + - + - w -" CHD_EOF
        /*5*/"- + - + - + - +" CHD_EOF
        /*4*/"+ - + - + - + -" CHD_EOF
        /*3*/"- + - W - + - +" CHD_EOF
        /*2*/"+ - + - + - + -" CHD_EOF
        /*1*/"- + - + - + B +" CHD_EOF
        /*0*/"w - + - + - + b"; 
    static const char * clean_board_8x8_01__b2 =     
        CHD_HDR_8X8_CLEAN_BOARD
        /*    0 1 2 3 4 5 6 7 */
        /*7*/"b + - + - + - +" CHD_EOF
        /*6*/"+ - + - + - w -" CHD_EOF
        /*5*/"- + - W - + - +" CHD_EOF      // difference there (5,3)
        /*4*/"+ - + - + - + -" CHD_EOF
        /*3*/"- + - W - + - +" CHD_EOF
        /*2*/"+ - + - + - + -" CHD_EOF
        /*1*/"- + - + - + B +" CHD_EOF
        /*0*/"w - + - + - + b"; 

    TEST(CheckersTests_GameModel, model_test_helpers_self_test__models_equal_test_02)
    {
        auto modelA = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01__b1);
        auto modelB = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01__b2);
        ASSERT_FALSE(TestModelsHelpers::are_models_equal(*modelA, *modelB));
    }
}

namespace test03 // Test for detecting differences in cells colors
{
    
    static const char * clean_board_8x8_01__b1 =     
        CHD_HDR_8X8_CLEAN_BOARD
        /*    0 1 2 3 4 5 6 7 */
        /*7*/"- + - + - + - +" CHD_EOF
        /*6*/"+ - + - + - + -" CHD_EOF
        /*5*/"- + - + - + - +" CHD_EOF
        /*4*/"+ - + - + - + -" CHD_EOF
        /*3*/"- + - + - + - +" CHD_EOF
        /*2*/"+ - + - + - + -" CHD_EOF
        /*1*/"- + - + - + - +" CHD_EOF
        /*0*/"+ - + - + - + -"; 
    static const char * clean_board_8x8_01__b2 =     
        CHD_HDR_8X8_CLEAN_BOARD
        /*    0 1 2 3 4 5 6 7 */
        /*7*/"- + - + - + - +" CHD_EOF
        /*6*/"+ - + - + - + -" CHD_EOF
        /*5*/"- + - + - + - +" CHD_EOF
        /*4*/"+ - + - + - + -" CHD_EOF
        /*3*/"- + - + - - - +" CHD_EOF // difference at (3, 5)
        /*2*/"+ - + - + - + -" CHD_EOF
        /*1*/"- + - + - + - +" CHD_EOF
        /*0*/"+ - + - + - + -"; 

    TEST(CheckersTests_GameModel, model_test_helpers_self_test__models_equal_test_03)
    {
        auto modelA = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01__b1);
        auto modelB = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01__b2);
        ASSERT_FALSE(TestModelsHelpers::are_models_equal(*modelA, *modelB));
    }
}

namespace test04 // Test for detecting differences in queens configuration
{
    
    static const char * clean_board_8x8_01__b1 =     
        CHD_HDR_8X8_CLEAN_BOARD
        /*    0 1 2 3 4 5 6 7 */
        /*7*/"- + - + - + - +" CHD_EOF
        /*6*/"+ - + - + - + -" CHD_EOF
        /*5*/"- + - + - + - +" CHD_EOF
        /*4*/"+ - + - + - + -" CHD_EOF
        /*3*/"- + - + - + - +" CHD_EOF
        /*2*/"+ - + - + - + -" CHD_EOF
        /*1*/"- b - + - + - +" CHD_EOF 
        /*0*/"+ - + - + - W -";        
    static const char * clean_board_8x8_01__b2 =     
        CHD_HDR_8X8_CLEAN_BOARD
        /*    0 1 2 3 4 5 6 7 */
        /*7*/"- + - + - + - +" CHD_EOF
        /*6*/"+ - + - + - + -" CHD_EOF
        /*5*/"- + - + - + - +" CHD_EOF
        /*4*/"+ - + - + - + -" CHD_EOF
        /*3*/"- + - + - + - +" CHD_EOF 
        /*2*/"+ - + - + - + -" CHD_EOF 
        /*1*/"- B - + - + - +" CHD_EOF // difference at (1, 0)
        /*0*/"+ - + - + - w -";        // difference at (0, 7)

    TEST(CheckersTests_GameModel, model_test_helpers_self_test__models_equal_test_04)
    {
        auto modelA = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01__b1);
        auto modelB = SimpleModelDeserizlier::deserialize_model(clean_board_8x8_01__b2);
        ASSERT_FALSE(TestModelsHelpers::are_models_equal(*modelA, *modelB));
    }
}


namespace test05 
{

    const char * board_8x8_clean =     
        CHD_HDR_8X8_DEFAULT_AUTO_BOARD
        /*    0 1 2 3 4 5 6 7 */
        /*7*/"- - - - - - - -" CHD_EOF
        /*6*/"- - - - - - - -" CHD_EOF
        /*5*/"- - - - - - - -" CHD_EOF
        /*4*/"- - - - - - - -" CHD_EOF
        /*3*/"- - - - - - - -" CHD_EOF
        /*2*/"- - - - - - - -" CHD_EOF
        /*1*/"- - - - - - - -" CHD_EOF
        /*0*/"- - - - - - - -"; 
    const char * board_8x8_one_figure =     
        CHD_HDR_8X8_DEFAULT_AUTO_BOARD
        /*    0 1 2 3 4 5 6 7 */
        /*7*/"- w - - - - - -" CHD_EOF
        /*6*/"- - - - - - - -" CHD_EOF
        /*5*/"- - - - - - - -" CHD_EOF
        /*4*/"- - - - - - - -" CHD_EOF
        /*3*/"- - - - - - - -" CHD_EOF
        /*2*/"- - - - - - - -" CHD_EOF
        /*1*/"- - - - - - - -" CHD_EOF
        /*0*/"- - - - - - - -"; 

    TEST(CheckersTests_GameModel, model_test_helpers_self_test__models_equal_test_04)
    {
        auto modelA = SimpleModelDeserizlier::deserialize_model(board_8x8_clean);
        auto modelB = SimpleModelDeserizlier::deserialize_model(board_8x8_one_figure);
        ASSERT_FALSE(TestModelsHelpers::are_models_equal(*modelA, *modelB)) << "Second board has one figure but it was not detected";
    }
}

