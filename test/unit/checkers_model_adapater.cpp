#include "checkers_model_adapater.h"

namespace checkers 
{
    namespace testing
    {
        
        BoardModel::Move BoardModel_LS::construct_model_move(ui_t r, ui_t c, ITesteeBoardModel::Move::Type type, 
            ui_t attacked_enemy_r, ui_t attacked_enemy_col )
        {
            return BoardModel::Move(encode_adaptee_move_type(type), BoardPos(r,c), BoardPos(attacked_enemy_r, attacked_enemy_col));
        }

        BoardModel::MoveType BoardModel_LS::encode_adaptee_move_type(ITesteeBoardModel::Move::Type mtype)
        {
            switch(mtype)
            {
            case ITesteeBoardModel::Move::Type_Move:
                {
                    return BoardModel::MoveType_Move;
                }
            case ITesteeBoardModel::Move::Type_Attack:
                {
                    return BoardModel::MoveType_Attack;                    
                }
            case ITesteeBoardModel::Move::Type_Invalid:
                {
                    return BoardModel::MoveType_Invalid;                    
                }
            default:
                throw std::logic_error("encode_adaptee_move_type:: Unknown move type.");
            }
        }

        ITesteeBoardModel::Move::Type BoardModel_LS::decode_adaptee_move_type(BoardModel::MoveType mtype)
        {
            switch(mtype)
            {
            case MoveType_Invalid: 
                {
                    return ITesteeBoardModel::Move::Type_Invalid;
                }
            case MoveType_Move: 
                {
                    return ITesteeBoardModel::Move::Type_Move;
                }
            case MoveType_Attack: 
                {
                    return ITesteeBoardModel::Move::Type_Attack;
                }
            default:
                throw std::logic_error("decode_adaptee_move_type:: Unknown move type.");
            }
        }

        void BoardModel_LS::generate_available_moves(ui_t row, ui_t col, bool attack_mode, ITesteeBoardModel::MovesCollection& out_moves) const
        {
            BoardModel::MovesCollection moves_internal;
            BoardModel::get_possible_moves(BoardPos(row, col), moves_internal, attack_mode);
            for (BoardModel::MovesCollection::const_iterator mit = moves_internal.begin(); mit != moves_internal.end(); ++mit)
            {
                ITesteeBoardModel::Move move;
                move.kind = decode_adaptee_move_type(mit->type());
                move.to_row = mit->position_to().row();
                move.to_col = mit->position_to().column();
                out_moves.push_back(move);
            }
        }

    }
}
