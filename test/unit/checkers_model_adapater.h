#ifndef CH_CHECKERS_MODEL_ADAPATER_H
#define CH_CHECKERS_MODEL_ADAPATER_H

#include <unit/framework/test_model_intf.h>
#include <common/board_model.h>

namespace checkers 
{
    namespace testing
    {

        // My board model adapted to interface of model which can be tested.
        class BoardModel_LS : public ITesteeBoardModel, private BoardModel
        {
        public:
            BoardModel_LS(unsigned rows, unsigned columns)
                : BoardModel(rows, columns)
            {}

            virtual void reset()
            {
                BoardModel::reset();
            }

            // Make cell white whatever is put on (becomes white empty).
            virtual void set_white_cell(ui_t row, ui_t col)
            {
                BoardModel::cell_at(BoardPos(row, col)) = Cell::get_empty_white();
            }
            
            // Make cell black whatever is put on (becomes black empty).
            virtual void set_black_cell(ui_t row, ui_t col)
            {
                BoardModel::cell_at(BoardPos(row, col)) = Cell::get_empty_black();
            }
            
            // Set white figure whatever is figure is put (preserves cell color).
            virtual void put_white_fig(ui_t row, ui_t col)
            {
                BoardModel::cell_at(BoardPos(row, col)).put_white_figure();
            }
            // Set black figure whatever is figure is put (preserves cell color).
            virtual void put_black_fig(ui_t row, ui_t col)
            {
                BoardModel::cell_at(BoardPos(row, col)).put_black_figure();
            }

            virtual void make_queen(ui_t row, ui_t col)
            {
                BoardModel::cell_at(BoardPos(row, col)).make_queen();
            }

            virtual bool is_empty_cell(ui_t row, ui_t col) const
            {
                return BoardModel::cell_at(BoardPos(row, col)).is_empty();
            }

            virtual bool is_queen(ui_t row, ui_t col) const
            {
                return BoardModel::cell_at(BoardPos(row, col)).is_queen();
            }

            virtual bool is_white_cell(ui_t row, ui_t col) const
            {
                return BoardModel::cell_at(BoardPos(row, col)).is_white_cell();
            }

            virtual bool is_black_cell(ui_t row, ui_t col) const
            {
                return BoardModel::cell_at(BoardPos(row, col)).is_black_cell();
            }

            virtual bool is_white_fig(ui_t row, ui_t col) const
            {
                return BoardModel::cell_at(BoardPos(row, col)).is_white_figure();
            }

            virtual bool is_black_fig(ui_t row, ui_t col) const
            {
                return BoardModel::cell_at(BoardPos(row, col)).is_black_figure();
            }

            virtual void generate_available_moves(ui_t row, ui_t col, bool attack_modde, ITesteeBoardModel::MovesCollection& out_moves) const;

            // Not implemented yet
            virtual bool is_attack_mode() const { throw std::runtime_error("not implemented yet"); }

            virtual unsigned rows_count() const
            {
                return BoardModel::rows_count();
            }
            // Returns of rows count of the board so that (columns_count() - 1) must be coordiate of last column of the board.
            virtual unsigned columns_count() const 
            {
                return BoardModel::columns_count();
            }

            virtual void remove_figure(ui_t row, ui_t col)
            {
                BoardModel::cell_at(BoardPos(row, col)).remove_figure();
            }

            virtual bool is_move_allowed(ui_t row_from, ui_t col_from, ui_t row_to, ui_t col_to, 
                ITesteeBoardModel::Move::Type type, bool attack_mode = false) const
            {
                return BoardModel::is_move_allowed(BoardPos(row_from, col_from), BoardPos(row_to, col_to), 
                    encode_adaptee_move_type(type), attack_mode);
            }

            virtual void make_move(ui_t row_from, ui_t col_from, ui_t row_to, ui_t col_to, bool attack_mode)
            {
                BoardModel::Move move = construct_model_move(row_to, col_to,  ITesteeBoardModel::Move::Type_Move, -1, -1);
                BoardModel::make_move(BoardPos(row_from, col_from), move, attack_mode);
            }
            
            virtual void make_attack(ui_t row_from, ui_t col_from, ui_t row_to, ui_t col_to, ui_t attacked_row, ui_t attacked_col, bool attack_mode)
            {
                BoardModel::Move move = construct_model_move(row_to, col_to,  ITesteeBoardModel::Move::Type_Attack, attacked_row, attacked_col);
                BoardModel::make_move(BoardPos(row_from, col_from), move, attack_mode);                
            }

            virtual unsigned blacks_figures_count() const
            {
                return BoardModel::black_figures_count();
            }

            virtual unsigned white_figures_count() const
            {
                return BoardModel::white_figures_count();
            }


        private:
            static ITesteeBoardModel::Move::Type decode_adaptee_move_type(BoardModel::MoveType mtype);
            static BoardModel::MoveType encode_adaptee_move_type(ITesteeBoardModel::Move::Type mtype);
            static BoardModel::Move construct_model_move(ui_t r, ui_t c, ITesteeBoardModel::Move::Type type, 
                ui_t attacked_enemy_r, ui_t attacked_enemy_col );

        };

    }
}


#endif // CH_CHECKERS_MODEL_ADAPATER_H

